# eslint-plugin-payloads

Button payloads checker

## Installation

You'll first need to install [ESLint](http://eslint.org):

```
$ npm i eslint --save-dev
```

Next, install `eslint-plugin-payloads`:

```
$ npm install eslint-plugin-payloads --save-dev
```

**Note:** If you installed ESLint globally (using the `-g` flag) then you must also install `eslint-plugin-payloads` globally.

## Usage

Add `payloads` to the plugins section of your `.eslintrc` configuration file. You can omit the `eslint-plugin-` prefix:

```json
{
    "plugins": [
        "payloads"
    ]
}
```





