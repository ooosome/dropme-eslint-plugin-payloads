const fs = require('fs');
const expect = require('chai').expect;

const payloadHelpers = require('../lib/payloadHelpers');

function readPayloadFile(fileName) {
  const payload = fs.readFileSync(`tests/resources/${fileName}`);
  return JSON.parse(payload);
}

describe('getPayloads', () => {
  it('gets buttons', () => {
    const payload = readPayloadFile('buttons.json');
    const buttons = payloadHelpers.getPayloads(payload);
    expect(buttons.payloads).to.deep.equal([
      'wb_28_bank_account_switch',
      'partner_ocbc_wb_13_bank_account_survey',
      '$return',
    ]);
  });

  it('gets switches', () => {
    const payload = readPayloadFile('switch.json');
    const buttons = payloadHelpers.getPayloads(payload);
    expect(buttons.payloads).to.deep.equal(['sw1', '$return', 'sw2']);
  });

  it('gets actions', () => {
    const payload = readPayloadFile('action.json');
    const buttons = payloadHelpers.getPayloads(payload);
    expect(buttons.payloads).to.deep.equal(['suc', 'unk', '$return']);
  });
});

describe('checkPayloadElement', () => {
  it('missing payloads - buttons', () => {
    const payload = readPayloadFile('buttons.json');
    const result = payloadHelpers.checkPayloadElement(payload, []);
    expect(result).has.lengthOf(2);
  });

  it('missing payloads - action', () => {
    const payload = readPayloadFile('action.json');
    const result = payloadHelpers.checkPayloadElement(payload, []);
    expect(result).has.lengthOf(2);
  });

  it('missing payloads - switch', () => {
    const payload = readPayloadFile('switch.json');
    const result = payloadHelpers.checkPayloadElement(payload, []);
    expect(result).has.lengthOf(2);
  });

  it('found payloads', () => {
    const payload = readPayloadFile('buttons.json');
    const result = payloadHelpers.checkPayloadElement(payload, [
      'wb_28_bank_account_switch.json',
      'partner_ocbc_wb_13_bank_account_survey.json',
      '$return',
    ]);
    expect(result).has.lengthOf(0);
  });

  it('found payloads', () => {
    const payload = readPayloadFile('buttons.json');
    const result = payloadHelpers.checkPayloadElement(payload, [
      'osome_wb_28_bank_account_switch.json',
      'osome_partner_ocbc_wb_13_bank_account_survey.json',
      '$return',
    ]);
    expect(result).has.lengthOf(0);
  });
});
