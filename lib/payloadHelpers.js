function checkPayloadElement(payloadPart, payloadFiles) {
  const { payloads, linterResults } = getPayloads(payloadPart);

  const payloadCheckResults = payloads
    .filter(payload => !checkIfPayloadFileIsPresent(payload, payloadFiles))
    .filter(payload => payload !== '$return' && payload !== 'bot')
    .map(payload => ({
      ruleId: 'json/unknown',
      severity: 2,
      message: `Missing file for "${payload}" payload`,
      line: 1,
      column: 1,
      endLine: 1,
      endColumn: 1,
      source: '',
    }));
  return [...payloadCheckResults, ...linterResults];
}

function getPayloads(payloadPart) {
  return [getAction, getButtons, getSwitch]
    .map(f => f(payloadPart))
    .reduce((prev, cur) => {
      const payloads = [...(prev.payloads || []), ...(cur.payloads || [])];
      const linterResults = [...(prev.linterResults || []), ...(cur.linterResults || [])];
      return { payloads, linterResults };
    }, {});
}

function getButtons(payloadPart) {
  if (!(payloadPart.attachment && payloadPart.attachment.payload && payloadPart.attachment.payload.elements)) {
    return {};
  }
  const payloads = payloadPart.attachment.payload.elements
    .filter(element => Boolean(element.buttons))
    .map(element => element.buttons)
    .reduce((prev, cur) => [...prev, ...cur], [])
    .filter(button => Boolean(button.payload))
    .map(button => button.payload);
  return { payloads };
}

function getSwitch(payloadPart) {
  if (!payloadPart.switch) {
    return {};
  }
  const linterResults = [];
  if (!payloadPart.switch.find(element => !Boolean(element.contexts))) {
    linterResults.push({
      ruleId: 'json/unknown',
      severity: 3,
      message: `Switch without default goto`,
      line: 1,
      column: 1,
      endLine: 1,
      endColumn: 1,
      source: '',
    });
  }
  const payloads = payloadPart.switch.map(element => element.goto || (element.gotoBot ? 'bot' : false));
  return { payloads, linterResults };
}

function getAction(payloadPart) {
  if (!payloadPart.action) {
    return {};
  }
  if (!payloadPart.action.cases) {
    return {
      linterResults: [
        {
          ruleId: 'json/unknown',
          severity: 3,
          message: `Action without cases`,
          line: 1,
          column: 1,
          endLine: 1,
          endColumn: 1,
          source: '',
        },
      ],
    };
  }
  const linterResults = [];
  if (!payloadPart.action.cases['UNKNOWN_ERROR']) {
    linterResults.push({
      ruleId: 'json/unknown',
      severity: 3,
      message: `Action without UNKNOWN_ERROR case`,
      line: 1,
      column: 1,
      endLine: 1,
      endColumn: 1,
      source: '',
    });
  }
  return { linterResults, payloads: Object.values(payloadPart.action.cases) };
}

function checkIfPayloadFileIsPresent(payload, payloadFiles) {
  const result = payloadFiles.some(file => file === `${payload}.json` || file === `osome_${payload}.json`);

  return result;
}

module.exports = { checkPayloadElement, getPayloads };
