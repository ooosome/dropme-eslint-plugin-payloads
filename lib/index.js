'use strict';

const fs = require('fs');
const path = require('path');

const payloadHelpers = require('./payloadHelpers');

var fileContents = {};

function getFilesFromDir(dir, fileTypes) {
  var filesToReturn = [];
  function walkDir(currentPath) {
    const files = fs.readdirSync(currentPath);
    files.forEach(file => {
      var curFile = path.join(currentPath, file);
      if (fs.statSync(curFile).isFile() && fileTypes.includes(path.extname(curFile))) {
        filesToReturn.push(curFile.replace(dir, '').substr(curFile.lastIndexOf('/') + 1, curFile.length));
      } else if (fs.statSync(curFile).isDirectory()) {
        walkDir(curFile);
      }
    });
  }

  walkDir(dir);
  return filesToReturn;
}

function getAllPayloadFiles() {
  return getFilesFromDir('./src/', ['.json']);
}

module.exports = {
  processors: {
    '.json': {
      preprocess: function(text, fileName) {
        fileContents[fileName] = JSON.parse(text);
        return [text];
      },
      postprocess: function(messages, fileName) {
        var contents = fileContents[fileName];
        delete fileContents[fileName];
        let formattedErrors = [];
        if (!Array.isArray(contents) || contents.length === 0) {
          formattedErrors.push({
            ruleId: 'json/unknown',
            severity: 2,
            message: 'File should contain a non-empty array!',
            line: 1,
            column: 1,
            endLine: 1,
            endColumn: 1,
            source: '',
          });
        }
        const payloadFiles = getAllPayloadFiles();
        formattedErrors = contents
          .map(element => payloadHelpers.checkPayloadElement(element, payloadFiles))
          .reduce((prev, cur) => [...prev, ...cur], formattedErrors);
        return formattedErrors;
      },
    },
  },
};
